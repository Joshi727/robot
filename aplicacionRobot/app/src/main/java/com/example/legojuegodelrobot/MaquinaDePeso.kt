package com.example.legojuegodelrobot

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MaquinaDePeso : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maquina_de_peso)

        val botonAnteriorQuince = findViewById<Button>(R.id.anteriorQuince)
        botonAnteriorQuince.setOnClickListener {
            val cambioAnteriorQuince = Intent(this, MaquinaDeRemos::class.java)
            startActivity(cambioAnteriorQuince)
        }

        val botonMenuQuince = findViewById<Button>(R.id.menuQuince)
        botonMenuQuince.setOnClickListener {
            val cambioMenuQuince = Intent(this, MainActivity::class.java)
            startActivity(cambioMenuQuince)
        }

        val botonSiguienteQuince = findViewById<Button>(R.id.siguienteQuince)
        botonSiguienteQuince.setOnClickListener {
            val cambioSiguienteQuince = Intent(this, UnidadesDeSalud::class.java)
            startActivity(cambioSiguienteQuince)
        }

    }
}
package com.example.legojuegodelrobot

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Tobogan : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tobogan)

        val botonAnteriorCinco = findViewById<Button>(R.id.anteriorCinco)
        botonAnteriorCinco.setOnClickListener {
            val cambioAnteriorCinco = Intent(this, ContadorDePasos::class.java)
            startActivity(cambioAnteriorCinco)
        }

        val botonMenuCinco = findViewById<Button>(R.id.menuCinco)
        botonMenuCinco.setOnClickListener {
            val cambioMenuCinco = Intent(this, MainActivity::class.java)
            startActivity(cambioMenuCinco)
        }

        val botonSiguienteCinco = findViewById<Button>(R.id.siguienteCinco)
        botonSiguienteCinco.setOnClickListener {
            val cambioSiguienteCinco = Intent(this, Banca::class.java)
            startActivity(cambioSiguienteCinco)
        }

    }
}
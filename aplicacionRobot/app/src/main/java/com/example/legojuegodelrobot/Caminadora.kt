package com.example.legojuegodelrobot

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Caminadora : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_caminadora)

        val buttonRevisar =findViewById<Button>(R.id.revisarCaminadora)
        buttonRevisar.setOnClickListener{
            val cambioRevisar = Intent(this, RevisarCaminadora::class.java)
            startActivity(cambioRevisar)
        }

        val botonAnteriorTrece = findViewById<Button>(R.id.anteriorTrece)
        botonAnteriorTrece.setOnClickListener {
            val cambioAnteriorTrece = Intent(this, telefono_celular::class.java)
            startActivity(cambioAnteriorTrece)
        }

        val botonMenuTrece = findViewById<Button>(R.id.menuTrece)
        botonMenuTrece.setOnClickListener {
            val cambioMenuTrece = Intent(this, MainActivity::class.java)
            startActivity(cambioMenuTrece)
        }

        val botonSiguienteTrece = findViewById<Button>(R.id.siguienteTrece)
        botonSiguienteTrece.setOnClickListener {
            val cambioSiguienteTrece = Intent(this, MaquinaDeRemos::class.java)
            startActivity(cambioSiguienteTrece)
        }

    }
}
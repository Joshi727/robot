package com.example.legojuegodelrobot

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class VolteoDeLlantas : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_volteo_de_llantas)

        val botonAnteriorOnce = findViewById<Button>(R.id.anteriorOnce)
        botonAnteriorOnce.setOnClickListener {
            val cambioAnteriorOnce = Intent(this, Petanca::class.java)
            startActivity(cambioAnteriorOnce)
        }

        val botonMenuOnce = findViewById<Button>(R.id.menuOnce)
        botonMenuOnce.setOnClickListener {
            val cambioMenuOnce = Intent(this, MainActivity::class.java)
            startActivity(cambioMenuOnce)
        }

        val botonSiguienteOnce = findViewById<Button>(R.id.siguienteOnce)
        botonSiguienteOnce.setOnClickListener {
            val cambioSiguienteOnce = Intent(this, telefono_celular::class.java)
            startActivity(cambioSiguienteOnce)
        }

    }
}
package com.example.legojuegodelrobot

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class RevisarCaminadora : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_revisar_caminadora)

        val botonAnteriorRegresa = findViewById<Button>(R.id.regresarCaminadora)
        botonAnteriorRegresa.setOnClickListener {
            val cambioAnteriorRegresa = Intent(this, Caminadora::class.java)
            startActivity(cambioAnteriorRegresa)
        }

    }
}
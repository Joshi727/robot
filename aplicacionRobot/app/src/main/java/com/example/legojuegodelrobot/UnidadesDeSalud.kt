package com.example.legojuegodelrobot

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class UnidadesDeSalud : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_unidades_de_salud)

        val botonAnteriorDieciseis = findViewById<Button>(R.id.anteriorDieciseis)
        botonAnteriorDieciseis.setOnClickListener {
            val cambioAnteriorDiecises = Intent(this, MaquinaDePeso::class.java)
            startActivity(cambioAnteriorDiecises)
        }

        val botonMenuDieciseis = findViewById<Button>(R.id.menuDieciseis)
        botonMenuDieciseis.setOnClickListener {
            val cambioMenuDieciseis = Intent(this, MainActivity::class.java)
            startActivity(cambioMenuDieciseis)
        }

        val botonSiguienteDieciseis = findViewById<Button>(R.id.siguienteDieciseis)
        botonSiguienteDieciseis.setOnClickListener {
            val cambioSiguienteDieciseis = Intent(this, Presicion::class.java)
            startActivity(cambioSiguienteDieciseis)
        }

    }
}
package com.example.legojuegodelrobot

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Presicion : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_presicion)

        val botonAnteriorDiecisiete = findViewById<Button>(R.id.anteriorDiecisiete)
        botonAnteriorDiecisiete.setOnClickListener {
            val cambioAnteriorDiecisiete = Intent(this, UnidadesDeSalud::class.java)
            startActivity(cambioAnteriorDiecisiete)
        }

        val botonMenuDiecisiete = findViewById<Button>(R.id.menuDiecisiete)
        botonMenuDiecisiete.setOnClickListener {
            val cambioMenuDiecisiete = Intent(this, MainActivity::class.java)
            startActivity(cambioMenuDiecisiete)
        }


    }
}
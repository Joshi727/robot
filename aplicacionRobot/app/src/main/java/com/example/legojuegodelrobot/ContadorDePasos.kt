package com.example.legojuegodelrobot

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class ContadorDePasos : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contador_de_pasos)

        val botonAnteriorCuatro = findViewById<Button>(R.id.anteriorCuatro)
        botonAnteriorCuatro.setOnClickListener {
            val cambioAnteriorCuatro = Intent(this, ProyectoDeInnovacion::class.java)
            startActivity(cambioAnteriorCuatro)
        }

        val botonMenuCuatro = findViewById<Button>(R.id.menuCuatro)
        botonMenuCuatro.setOnClickListener {
            val cambioMenuCuatro = Intent(this, MainActivity::class.java)
            startActivity(cambioMenuCuatro)
        }

        val botonSiguienteCuatro = findViewById<Button>(R.id.siguienteCuatro)
        botonSiguienteCuatro.setOnClickListener {
            val cambioSiguienteCuatro = Intent(this, Tobogan::class.java)
            startActivity(cambioSiguienteCuatro)
        }

    }
}
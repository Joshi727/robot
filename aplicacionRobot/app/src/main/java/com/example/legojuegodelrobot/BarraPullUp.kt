package com.example.legojuegodelrobot

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class BarraPullUp : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_barra_pull_up)

        val botonAnteriorOcho = findViewById<Button>(R.id.anteriorOcho)
        botonAnteriorOcho.setOnClickListener {
            val cambioAnteriorOcho = Intent(this, Canasta::class.java)
            startActivity(cambioAnteriorOcho)
        }

        val botonMenuOcho = findViewById<Button>(R.id.menuOcho)
        botonMenuOcho.setOnClickListener {
            val cambioMenuOcho = Intent(this, MainActivity::class.java)
            startActivity(cambioMenuOcho)
        }

        val botonSiguienteOcho = findViewById<Button>(R.id.siguienteOcho)
        botonSiguienteOcho.setOnClickListener {
            val cambioSiguienteOcho = Intent(this, BaileDelRobot::class.java)
            startActivity(cambioSiguienteOcho)
        }

    }
}
package com.example.legojuegodelrobot

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class BaileDelRobot : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_baile_del_robot)

        val botonAnteriorNueve = findViewById<Button>(R.id.anteriorNueve)
        botonAnteriorNueve.setOnClickListener {
            val cambioAnteriorNueve = Intent(this, BarraPullUp::class.java)
            startActivity(cambioAnteriorNueve)
        }

        val botonMenuNueve = findViewById<Button>(R.id.menuNueve)
        botonMenuNueve.setOnClickListener {
            val cambioMenuNueve = Intent(this, MainActivity::class.java)
            startActivity(cambioMenuNueve)
        }

        val botonSiguienteNueve = findViewById<Button>(R.id.siguienteNueve)
        botonSiguienteNueve.setOnClickListener {
            val cambioSiguienteNueve = Intent(this, Petanca::class.java)
            startActivity(cambioSiguienteNueve)
        }

    }
}
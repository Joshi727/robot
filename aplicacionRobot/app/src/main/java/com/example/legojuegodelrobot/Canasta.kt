package com.example.legojuegodelrobot

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Canasta : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_canasta)

        val botonAnteriorSiete = findViewById<Button>(R.id.anteriorSiete)
        botonAnteriorSiete.setOnClickListener {
            val cambioAnteriorSiete = Intent(this, Banca::class.java)
            startActivity(cambioAnteriorSiete)
        }

        val botonMenuSiete = findViewById<Button>(R.id.menuSiete)
        botonMenuSiete.setOnClickListener {
            val cambioMenuSiete = Intent(this, MainActivity::class.java)
            startActivity(cambioMenuSiete)
        }

        val botonSiguienteSiete = findViewById<Button>(R.id.siguienteSiete)
        botonSiguienteSiete.setOnClickListener {
            val cambioSiguienteSiete = Intent(this, BarraPullUp::class.java)
            startActivity(cambioSiguienteSiete)
        }

    }
}
package com.example.legojuegodelrobot

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class ProyectoDeInnovacion : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_proyecto_de_innovacion)

        val botonAnteriorTres = findViewById<Button>(R.id.anteriorTres)
        botonAnteriorTres.setOnClickListener {
            val cambioAnteriorTres = Intent(this, M00Equipamento::class.java)
            startActivity(cambioAnteriorTres)
        }

        val botonMenuTres = findViewById<Button>(R.id.menuTres)
        botonMenuTres.setOnClickListener {
            val cambioMenuTres = Intent(this, MainActivity::class.java)
            startActivity(cambioMenuTres)
        }

        val botonSiguienteTres = findViewById<Button>(R.id.siguienteTres)
        botonSiguienteTres.setOnClickListener {
            val cambioSiguienteTres = Intent(this, ContadorDePasos::class.java)
            startActivity(cambioSiguienteTres)
        }

    }
}
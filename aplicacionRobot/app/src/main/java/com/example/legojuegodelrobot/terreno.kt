package com.example.legojuegodelrobot

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class terreno : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terreno)

        val botonMenuUno = findViewById<Button>(R.id.menuUno)
        botonMenuUno.setOnClickListener {
            val cambioMenuUno = Intent(this, MainActivity::class.java)
            startActivity(cambioMenuUno)
        }

        val botonSiguienteUno = findViewById<Button>(R.id.siguienteUno)
        botonSiguienteUno.setOnClickListener {
            val cambioSiguienteUno = Intent(this, M00Equipamento::class.java)
            startActivity(cambioSiguienteUno)
        }

    }
}
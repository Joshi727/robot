package com.example.legojuegodelrobot

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class M00Equipamento : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_m00_equipamento)

        val botonAnteriorDos = findViewById<Button>(R.id.anteriorDos)
        botonAnteriorDos.setOnClickListener {
            val cambioAnteriorDos = Intent(this, terreno::class.java)
                startActivity(cambioAnteriorDos)
        }

        val botonMenuDos = findViewById<Button>(R.id.menuDos)
        botonMenuDos.setOnClickListener {
            val cambioMenuDos = Intent(this, MainActivity::class.java)
            startActivity(cambioMenuDos)
        }

        val botonSiguienteDos = findViewById<Button>(R.id.siguienteDos)
        botonSiguienteDos.setOnClickListener {
            val cambioSiguienteDos = Intent(this, ProyectoDeInnovacion::class.java)
            startActivity(cambioSiguienteDos)
        }
    }
}
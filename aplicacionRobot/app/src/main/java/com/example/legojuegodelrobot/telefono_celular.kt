package com.example.legojuegodelrobot

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class telefono_celular : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_telefono_celular2)

        val botonAnteriorDoce = findViewById<Button>(R.id.anteriorDoce)
        botonAnteriorDoce.setOnClickListener {
            val cambioAnteriorDoce = Intent(this, VolteoDeLlantas::class.java)
            startActivity(cambioAnteriorDoce)
        }

        val botonMenuDoce = findViewById<Button>(R.id.menuDoce)
        botonMenuDoce.setOnClickListener {
            val cambioMenuDoce = Intent(this, MainActivity::class.java)
            startActivity(cambioMenuDoce)
        }

        val botonSiguienteDoce = findViewById<Button>(R.id.siguienteDoce)
        botonSiguienteDoce.setOnClickListener {
            val cambioSiguienteDoce = Intent(this, Caminadora::class.java)
            startActivity(cambioSiguienteDoce)
        }

    }
}
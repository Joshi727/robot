package com.example.legojuegodelrobot

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //boton para cambiar de actividad terreno de juego
        val terrenoJuego =findViewById<Button>(R.id.terrenoJuego)
        terrenoJuego.setOnClickListener{
        val cambio =Intent(this, terreno::class.java)
            startActivity(cambio)
        }
        //boton para cambiar actividad de equipamiento
        val buttonEquipamiento =findViewById<Button>(R.id.m00Equipamiento)
        buttonEquipamiento.setOnClickListener{
            val cambioEquipamento= Intent(this, M00Equipamento::class.java)
                startActivity(cambioEquipamento)
        }
        //boton para cambiar de actividad innovación
        val buttonInnovacion = findViewById<Button>(R.id.m01Inovacion)
        buttonInnovacion.setOnClickListener{
            val cambioInnovacion = Intent(this, ProyectoDeInnovacion::class.java)
                startActivity(cambioInnovacion)
        }
        //boton para cambiar actividad contador de pasos
        val buttonContador=findViewById<Button>(R.id.m002Pasos)
        buttonContador.setOnClickListener {
            val cambioContador = Intent(this, ContadorDePasos::class.java)
                startActivity(cambioContador)
        }
        //boton para cambiar actividad tobogán
        val buttonTobogan=findViewById<Button>(R.id.m003Tobogan)
        buttonTobogan.setOnClickListener {
            val cambioTobogan=Intent(this,Tobogan::class.java)
                startActivity(cambioTobogan)
        }
        //boton para cambiar actividad banca
        val buttonBanca = findViewById<Button>(R.id.m04Banca)
        buttonBanca.setOnClickListener {
            val cambioBanca=Intent(this,Banca::class.java)
                startActivity(cambioBanca)
        }
        //boton para cambiar actividad canasta
        val buttonCanasta = findViewById<Button>(R.id.m05Canasta)
        buttonCanasta.setOnClickListener {
            val cambioCanasta= Intent(this,Canasta::class.java)
                startActivity(cambioCanasta)
        }
        //boton para cambiar actividad barra pull-up
        val buttonBarra = findViewById<Button>(R.id.m06Barra)
        buttonBarra.setOnClickListener {
            val cambioBarra= Intent(this,BarraPullUp::class.java)
                startActivity(cambioBarra)
        }
        //boton para cambiar actividad baile del robot
        val buttonBaile = findViewById<Button>(R.id.m07Baile)
        buttonBaile.setOnClickListener {
            val cambioBaile = Intent(this,BaileDelRobot::class.java)
                startActivity(cambioBaile)
        }
        //boton para cambiar actividad petanca
        val buttonPetanca = findViewById<Button>(R.id.m08Petanca)
        buttonPetanca.setOnClickListener {
            val cambioPetanca = Intent(this, Petanca::class.java)
                startActivity(cambioPetanca)
        }

        val buttonVolteo = findViewById<Button>(R.id.m09Volteo)
        buttonVolteo.setOnClickListener {
            val cambioVolteo = Intent(this,VolteoDeLlantas::class.java)
                startActivity(cambioVolteo)
        }

        val buttonTelefono = findViewById<Button>(R.id.m10Telefono)
        buttonTelefono.setOnClickListener {
            val cambioTelefono = Intent(this,telefono_celular::class.java)
                startActivity(cambioTelefono)
        }
        val buttonCaminadora = findViewById<Button>(R.id.m11Caminadora)
        buttonCaminadora.setOnClickListener {
            val cambioCaminadora = Intent(this, Caminadora::class.java)
                startActivity(cambioCaminadora)
        }

        val buttonMaquinaRemos = findViewById<Button>(R.id.m12Maquina)
        buttonMaquinaRemos.setOnClickListener {
            val cambioMaquinaRemos = Intent(this,MaquinaDeRemos::class.java)
                startActivity(cambioMaquinaRemos)
        }

        val buttonMaquinaDePeso = findViewById<Button>(R.id.m13MaquinaPeso)
        buttonMaquinaDePeso.setOnClickListener {
            val cambioMaquinaPeso = Intent(this,MaquinaDePeso::class.java)
                startActivity(cambioMaquinaPeso)
        }

        val buttonUnidades = findViewById<Button>(R.id.m14Unidades)
        buttonUnidades.setOnClickListener {
            val cambioUnidades = Intent(this,UnidadesDeSalud::class.java)
                startActivity(cambioUnidades)
        }

        val buttonPrecision = findViewById<Button>(R.id.m15Percision)
        buttonPrecision.setOnClickListener {
            val cambioPrecision = Intent(this,Presicion::class.java)
                startActivity(cambioPrecision)
        }

    }
}
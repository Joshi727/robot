package com.example.legojuegodelrobot

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Petanca : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_petanca)

        val botonAnteriorDiez = findViewById<Button>(R.id.anteriorDiez)
        botonAnteriorDiez.setOnClickListener {
            val cambioAnteriorDiez = Intent(this, BaileDelRobot::class.java)
            startActivity(cambioAnteriorDiez)
        }

        val botonMenuDiez = findViewById<Button>(R.id.menuDiez)
        botonMenuDiez.setOnClickListener {
            val cambioMenuDiez = Intent(this, MainActivity::class.java)
            startActivity(cambioMenuDiez)
        }

        val botonSiguienteDiez = findViewById<Button>(R.id.siguienteDiez)
        botonSiguienteDiez.setOnClickListener {
            val cambioSiguienteDiez = Intent(this, VolteoDeLlantas::class.java)
            startActivity(cambioSiguienteDiez)
        }

    }
}
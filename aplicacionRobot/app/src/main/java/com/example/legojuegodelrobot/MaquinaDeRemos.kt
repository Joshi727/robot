package com.example.legojuegodelrobot

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MaquinaDeRemos : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maquina_de_remos)

        val botonAnteriorCatorce = findViewById<Button>(R.id.anteriorCatorce)
        botonAnteriorCatorce.setOnClickListener {
            val cambioAnteriorCatorce = Intent(this, Caminadora::class.java)
            startActivity(cambioAnteriorCatorce)
        }

        val botonMenuCatorce = findViewById<Button>(R.id.menuCatorce)
        botonMenuCatorce.setOnClickListener {
            val cambioMenuCatorce = Intent(this, MainActivity::class.java)
            startActivity(cambioMenuCatorce)
        }

        val botonSiguienteCatorce = findViewById<Button>(R.id.siguienteCatorce)
        botonSiguienteCatorce.setOnClickListener {
            val cambioSiguienteCatorce = Intent(this, MaquinaDePeso::class.java)
            startActivity(cambioSiguienteCatorce)
        }

    }
}
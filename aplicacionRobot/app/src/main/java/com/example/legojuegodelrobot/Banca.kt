package com.example.legojuegodelrobot

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Banca : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_banca)

        val botonAnteriorSeis = findViewById<Button>(R.id.anteriorSeis)
        botonAnteriorSeis.setOnClickListener {
            val cambioAnteriorSeis = Intent(this, Tobogan::class.java)
            startActivity(cambioAnteriorSeis)
        }

        val botonMenuSeis = findViewById<Button>(R.id.menuSeis)
        botonMenuSeis.setOnClickListener {
            val cambioMenuSeis = Intent(this, MainActivity::class.java)
            startActivity(cambioMenuSeis)
        }

        val botonSiguienteSeis = findViewById<Button>(R.id.siguienteSeis)
        botonSiguienteSeis.setOnClickListener {
            val cambioSiguienteSeis = Intent(this, Canasta::class.java)
            startActivity(cambioSiguienteSeis)
        }

    }
}